"Day 7: Arrays"

#!/bin/python3

import math
import os
import random
import re
import sys


def reverse_list(array):
    return array[::-1]


if __name__ == '__main__':
    n = int(input())

    arr = list(map(int, input().rstrip().split()))
    result = ""
    for item in reverse_list(arr):
        result = result + str(item) + " "
    print(result)