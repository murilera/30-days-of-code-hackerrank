"Day 8: Dictionaries and Maps"
# Enter your code here. Read input from STDIN. Print output to STDOUT

phoneBook = {}

def add_phone_number(data):
    name_number = data.split(" ")
    phoneBook[name_number[0]] = name_number[1]

def search_phone_book(name):
    number = phoneBook.get(name)
    if number:
        return f'{name}={number}'
    return 'Not found'

value = input()
a = []
while (value):
    a.append(value)
    try:
        value = input()
    except EOFError:
        break

entrys = int(a.pop(0))

for i in range(entrys):
    add_phone_number(a[i])

for i in range(entrys, len(a)):
    print(search_phone_book(a[i]))