"Day 6: Let's Review"

# Enter your code here. Read input from STDIN. Print output to STDOUT

def even_odd_elements(input_list):
    even_list = []
    even_word = ""
    odd_list = []
    odd_word = ""

    for i in range(len(input_list)):
        if i % 2 == 0:
            even_list.append(input_list[i])
        if i % 2 > 0:
            odd_list.append(input_list[i])
    for item in even_list:
        even_word = even_word + item
    for item in odd_list:
        odd_word = odd_word + item

    result = f'{even_word} {odd_word}'
    return result

value = input()
a = []
while (value):
    a.append(value)
    try:
        value = input()
    except EOFError:
        break

for i in range(int(a[0])):
    result = even_odd_elements(a[i+1])
    print(result)